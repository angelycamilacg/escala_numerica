![Escala numerica](http://www.madarme.co/portada-web.png)


# Titulo del proyecto: escala numerica

Proyecto en el cual se realiza un algoritmo para hallar la escala numérica.

# Tabla de contenido
1. [Escala numérica](#título-del-proyecto-escala-numérica)
2. [Ejemplo](#ejemplo)
3. [Características](#características)
4. [Tecnologías](#tecnologías)
5. [IDE](#IDE)
6. [Demo](#demo)
7. [Contribución](#contribución)
8. [Autor](#autor)
9. [Institución Educativa](#institucion-educativa)

## Ejemplo
A continuación un ejemplo del funcionamiento de una escala numérica.
Se muestran los datos referentes escala numérica: 1 

>Operación multiplicar 

| Dato 1 | Dato 2 | Resultado |
|--------|--------|-----------|
| 1      | 1      | 1         |
| 1      | 2      | 2         |
| 2      | 3      | 6         |
| 6      | 4      | 24        |
| 24     | 5      | 120       |
| 120    | 6      | 720       |
| 720    | 7      | 5040      |
| 5040   | 8      | 40320     |
| 40320  | 9      | 362880    |
| 362880 | 10     | 362880    |

>Operación dividir

| Dato 1 | Dato 2 | Resultado |
|--------|--------|-----------|
| 1      | 1      | 1         |
| 1      | 2      | 2         |
| 2      | 3      | 6         |
| 6      | 4      | 24        |
| 24     | 5      | 120       |
| 120    | 6      | 720       |
| 720    | 7      | 5040      |
| 5040   | 8      | 40320     |
| 40320  | 9      | 362880    |
| 362880 | 10     | 362880    |


## Características
El programa recibe un número y realiza la operación multiplicar de 1 hasta 10. Con el último resultado de la operación, se empiza la operación dividir, la cual al final debe dar el número inicial.

